/*
 *
 * My32x15Font
 *
 * Created manually with inspiration and help of MyBigFont, originally published by Brissieboy
 * and featured on the Freetronics forums http://forum.freetronics.com/viewtopic.php?f=26&t=5754
 *
 * This is a special character set created to utilise the full 32 pixel height of two stacked Freetronics DMD displays
 * for use in large outdoor displays, scoreboards etc that need to be viewable from a distance.  The font is 
 * used in the standard (landscape) orientation of the DMD panel, to avoid reducing viewing angle.
 *
 * Freetronics DMD see http://www.freetronics.com/products/dot-matrix-display-32x16-red
 *  or http://www.jaycar.com.au/productView.asp?ID=XC4250
 * 
 * As per the MyBigFont example, only characters 32 (space) to 95 (underscore) are defined - no lower case.
  *
 * See comments throughout for construction of file and the characters
 *
 * File Name           : My32x15Font.h
 * Date                : 29 Jan 2016
 * Font size in bytes  : 1663
 * Font width          : 15
 * Font height         : 32
 * Font first char     : 32
 * Font last char      : 95
 * Font used chars     : 64
 *
 * The font data are defined as
 *
 * struct _FONT_ {
 *     uint16_t   font_Size in Bytes over all included Size it self - BUT IT IS NOT USED IN THE ARDUINO DMD LIBRARY
 *                except in the case of 0x00 0x00 which signifies a fixed width character set
 *     uint8_t    font_Width_in_Pixel_for_fixed_drawing - only used for fixed width font where no character width table is allowed
 *     uint8_t    font_Height_in_Pixel_for_all_characters - only used for fixed width fonts
 *                but is usually set to 'en' width for proportional spaced fonts
 *     unit8_t    font_First_Char
 *     uint8_t    font_Char_Count
 *     uint8_t    font_Char_Widths[font_Last_Char - font_First_Char +1]
 *                1 entry per character indicating its width in pixels,
 *                characters < 128 have an implicit virtual right empty row for character spacing
 *                (This is built into the DMD library)
 *     uint8_t    font_data[];
 *                bit field of all characters
 *                The number of bytes must equal the entry in the width table
 */

#include <inttypes.h>
#ifdef __AVR__
#include <avr/pgmspace.h>
#elif defined (ESP8266)
#include <pgmspace.h>
#else
#define PROGMEM
#endif

#ifndef MY32X8FONT_H
#define MY32X8FONT_H

#define MY32X8FONT_WIDTH 15
#define MY32X8FONT_HEIGHT 32

const static uint8_t My32x8Font[] PROGMEM = { 						// the name here 'My32x15Font' must be used to refer to this font
    0x04, 0x90, 													// size     the total size of the font in bytes   = 1168 bytes
    0x0F, 															// width          not actually used in this case but is set to en width (width of 'n' character)
																	//                but being non-zero indicates that it is a variable width font and
																	//                and the character width table is present.
    0x20, 															// height         max height of the characters = 32
    0x20, 															// first char     hex value of the first character (space)
    0x40, 															// char count     the number of characters in this font

    // char widths 
	0x03, 0x05, 0x07, 0x0A, 0x0F, 0x0B, 0x0F, 0x03, 0x09, 0x09,     // 32 to 41 space to )
	0x09, 0x0C, 0x05, 0x0C, 0x02, 0x0F, 0x07, 0x05, 0x07, 0x07,		// 42 to 51 * to 3
	0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x05, 0x05, 0x09, 0x09, 	// 52 to 61 4 to =
    0x09, 0x0F, 0x0F, 0x0F, 0x07, 0x07, 0x07, 0x06, 0x0F, 0x08, 	// 62 to 71 > to G
	0x0F, 0x02, 0x07, 0x0F, 0x06, 0x0F, 0x08, 0x06, 0x06, 0x0F,		// 72 to 81 H to Q
	0x06, 0x08, 0x06, 0x0F, 0x0F, 0x08, 0x0F, 0x0F, 0x0F, 0x09,		// 82 to 91 R to [
	0x0F, 0x09, 0x07, 0x0C,											// 92 to 95 \ to _

    // font data
	//	col0................col1....................col2....................col4....................col5....................etc	
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 32 space (only 3 pixels wide which is less than normal)
	0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0x3F, 0xFF, 0x3F, 0x0F, 0x70, 0xF8, 0xF8, 0xF8, 0x70,   // 33 !
    0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x07, 0x0F, 0x07, 0x00, 0x07, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // 34 "
 	0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x1C, 0x1C, 0xFF, 0xFF, 0x1C, 0x1C, 0xFF, 0xFF, 0x1C, 0x1C, 0x0E, 0x0E, 0xFF, 0xFF, 0x0E, 0x0E, 0xFF, 0xFF, 
	0x0E, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 35 #
	0xF8, 0xFC, 0xFE, 0xFE, 0x1E, 0x0E, 0xFF, 0xFF, 0xFF, 0x0E, 0x0E, 0x1E, 0x1E, 0x3E, 0x7C, 0x7F, 0xFF, 0xFF, 0xFF, 0xE0, 0xC0, 0xFF, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0x80, 
	0x80, 0x00, 0x00, 0x01, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xFF, 0x03, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0x3E, 0x7C, 0x78, 0x78, 0x70, 0x70, 0xFF, 0xFF, 0xFF, 0x70, 0x78, 
	0x7F, 0x7F, 0x3F, 0x1F,   // 36 $
	0x70, 0x88, 0x88, 0x88, 0x70, 0x00, 0x80, 0xF8, 0xFF, 0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xFC, 0xFF, 0x3F, 0x07, 0x00, 0x00, 0x00, 0x80, 0xF8, 0xFF, 0xFF, 0x0F, 
	0x01, 0x80, 0x80, 0x80, 0x00, 0x78, 0x7F, 0x7F, 0x0F, 0x00, 0x00, 0x07, 0x08, 0x08, 0x08, 0x07,   // 37 %
    0x00, 0x80, 0xC0, 0xC0, 0x60, 0x70, 0x70, 0x70, 0xE0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x7F, 0xE1, 0xC0, 0x80, 0xC0, 0xC0, 0x70, 0x7C, 0x1F, 0x07, 0x00, 0x00, 
	0x00, 0x00, 0x7C, 0xFE, 0x87, 0x03, 0x01, 0x03, 0x07, 0x0E, 0xDC, 0xF8, 0x70, 0xF8, 0xDC, 0x0C, 0x00, 0x00, 0x03, 0x07, 0x0E, 0x0C, 0x1C, 0x1E, 0x0F, 0x07, 0x01, 0x00, 
	0x01, 0x03, 0x0F, 0x06,   // 38 &
    0xFF, 0xFF, 0xFF, 0x07, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 39 '
    0xF0, 0xFC, 0xFE, 0xFE, 0x1F, 0x0F, 0x07, 0x03, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 
	0x3F, 0x7F, 0x7F, 0xF8, 0xF0, 0xE0, 0xC0, 0x80,   // 40 (
    0x01, 0x03, 0x07, 0x0F, 0x1F, 0xFE, 0xFE, 0xFC, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x80, 
	0xC0, 0xE0, 0xF0, 0xF8, 0x7F, 0x7F, 0x3F, 0x0F,   // 41 )
    0x01, 0x82, 0x44, 0x28, 0xFF, 0x28, 0x44, 0x82, 0x01, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 42 *
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0xFC, 0xFC, 0xFC, 0xFC, 0xC0, 0xC0, 0xC0, 0xC0, 0x03, 0x03, 0x03, 0x03, 
	0x3F, 0x3F, 0x3F, 0x3F, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 43 +
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x00, 0xC7, 0xEF, 0x7F, 0x3F, 0x1F,   // 44 ,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0x03, 0x03, 0x03, 0x03, 
	0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 45 -
	0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 46 .
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 
	0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00,   // 47 /
    0xF8, 0xFC, 0x0E, 0x06, 0x0E, 0xFC, 0xF8, 0x07, 0x0F, 0x1C, 0x18, 0x1C, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 48 0
    0x30, 0x18, 0x0C, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 49 1
    0x18, 0x1C, 0x06, 0x86, 0xC6, 0xFC, 0x38, 0x1C, 0x1E, 0x1B, 0x1B, 0x19, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 50 2
   0x18, 0x1C, 0xC6, 0xC6, 0xE6, 0xBC, 0x18, 0x0C, 0x1C, 0x18, 0x10, 0x18, 0x1F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 51 3
   0x00, 0xC0, 0x70, 0x1C, 0xFE, 0xFE, 0x00, 0x07, 0x07, 0x06, 0x06, 0x1F, 0x1F, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 52 4
   0xF0, 0xFE, 0x4E, 0x66, 0xE6, 0xC6, 0x80, 0x0C, 0x1C, 0x18, 0x18, 0x1C, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 53 5
   0xF0, 0xFC, 0xCE, 0x66, 0xE6, 0xCE, 0x8C, 0x03, 0x0F, 0x1C, 0x18, 0x18, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 54 6
  0x06, 0x06, 0xC6, 0xF6, 0x3E, 0x0E, 0x06, 0x00, 0x1E, 0x1F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 55 7
   0x38, 0xFC, 0xC6, 0xC6, 0xC6, 0xFC, 0x38, 0x07, 0x0F, 0x18, 0x18, 0x18, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 56 8
   0x78, 0xFC, 0xC6, 0x86, 0xCE, 0xFC, 0xF8, 0x0C, 0x1C, 0x19, 0x19, 0x1C, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 57 9
    0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x7C, 0x7C, 0x7C, 0x38, 0x38, 0x7C, 0x7C, 0x7C, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00,   // 58 :
    0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x7C, 0x7C, 0x7C, 0x38, 0x38, 0x7C, 0xFC, 0xFC, 0xF8, 0x06, 0x07, 0x03, 0x01, 0x00,   // 59 ;
    0x00, 0x00, 0x00, 0x00, 0x80, 0xC0, 0xE0, 0x70, 0x38, 0x80, 0xE0, 0xF8, 0x7E, 0x1F, 0x07, 0x01, 0x00, 0x00, 0x01, 0x07, 0x1F, 0x7E, 0xF8, 0xE0, 0x80, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x01, 0x03, 0x07, 0x0E, 0x1C,   // 60 <
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 61 =
    0x38, 0x70, 0xE0, 0xC0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x07, 0x1F, 0x7E, 0xF8, 0xE0, 0x80, 0x00, 0x00, 0x80, 0xE0, 0xF8, 0x7E, 0x1F, 0x07, 0x01, 0x1C, 
	0x0E, 0x07, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00,   // 62 >
    0x7C, 0x7E, 0x7F, 0x7F, 0x1F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x1F, 0xFF, 0xFF, 0xFE, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xE0, 0xFF, 0xFF, 
	0xFF, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0x7F, 0x7F, 0x7F, 0x03, 0x03, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0xFC, 0xFC, 0xFC, 0xFC, 0x78, 0x00, 
	0x00, 0x00, 0x00, 0x00,   // 63 ?
    0xFC, 0xFE, 0xFF, 0xFF, 0x1F, 0x0F, 0x0F, 0x0F, 0x0F, 0x8F, 0x9F, 0xFF, 0xFF, 0xFE, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFE, 0xFF, 0xC7, 0x01, 0x83, 0xC3, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x01, 0x03, 0x23, 0x63, 0x63, 0xE3, 0xC1, 0x00, 0x3F, 0x7F, 0xFF, 0xFF, 0xF8, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF8, 
	0xFC, 0xFF, 0x7F, 0x3F,   // 64 @
	0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0x1E, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xF0, 0xFC, 0xFF, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x03, 0x0F, 0xFF, 0xFC, 
	0xF0, 0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x7F, 0x7F, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x7F, 0x7F, 0x7F, 0x7F,   // 65 A
    0xFE, 0xFE, 0xC6, 0xC6, 0xEE, 0x3C, 0x18, 0x1F, 0x1F, 0x18, 0x18, 0x1D, 0x0F, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 66 B
    0xFC, 0xFE, 0x0E, 0x06, 0x06, 0x0E, 0x0C, 0x0F, 0x1F, 0x1C, 0x18, 0x18, 0x1C, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 67 C
	0xFE, 0xFE, 0x06, 0x06, 0x06, 0xFE, 0xFC, 0x1F, 0x1F, 0x18, 0x18, 0x18, 0x1F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 	// 68 D
    0xFE, 0xFE, 0xC6, 0xC6, 0xC6, 0xC6, 0x1F, 0x1F, 0x18, 0x18, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 69 E
    0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0x00, 0x00, 
	0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00,   // 70 F
    0xF8, 0xFC, 0x0E, 0x06, 0xC6, 0xCE, 0xDC, 0x98, 0x07, 0x0F, 0x1C, 0x18, 0x18, 0x1C, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 71 G
    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0xFF, 0xFF, 0xFF, 0xFF,   // 72 H
	0xFE, 0xFE, 0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00,   // 73 I
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0xFE, 0x0E, 0x1E, 0x18, 0x18, 0x18, 0x1F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 74 J
    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0x00, 0x00, 
	0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0x0F, 0x3F, 0xFF, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F, 0x3F, 
	0xFF, 0xFC, 0xF0, 0xC0,   // 75 K
    0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x18, 0x18, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // 76 L
    0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xF8, 0xE0, 0xF8, 0xFF, 0x3F, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x01, 0x07, 0x1F, 0x07, 0x01, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0xFF, 0xFF, 0xFF, 0xFF,   // 77 M
    0xFE, 0xFE, 0x0E, 0xFC, 0xF0, 0x00, 0xFE, 0xFE, 0x1F, 0x1F, 0x00, 0x03, 0x0F, 0x1C, 0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 78 N
    0xFC, 0xFE, 0x06, 0x06, 0xFE, 0xFC, 0x0F, 0x1F, 0x18, 0x18, 0x1F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// 79 O
    0xFE, 0xFE, 0xC6, 0xC6, 0xFE, 0x7C, 0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 80 P
    0xFC, 0xFE, 0xFF, 0xFF, 0x1F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x1F, 0xFF, 0xFF, 0xFE, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x80, 0x80, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0x7F, 0xFF, 0xFF, 0xF8, 0xF0, 0xF0, 0xF3, 0xF3, 0xF7, 0x7E, 
	0x7F, 0x3F, 0x7F, 0xE7,   // 81 Q
    0xFE, 0xFE, 0xC6, 0xC6, 0x7E, 0x3C, 0x1F, 0x1F, 0x00, 0x03, 0x1F, 0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 82 R
    0x38, 0x7C, 0xEE, 0xC6, 0xC6, 0xCE, 0x9C, 0x18, 0x06, 0x0E, 0x1C, 0x18, 0x18, 0x1D, 0x0F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 83 S
	0x06, 0x06, 0xFE, 0xFE, 0x06, 0x06, 0x00, 0x00, 0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 84 T
    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0x7F, 0xFF, 0xFF, 0xF8, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF8, 
	0xFF, 0xFF, 0x7F, 0x3F,   // 85 U
    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xFF, 0xFF, 
	0xFF, 0xFF, 0x00, 0x0F, 0x3F, 0xFF, 0xFF, 0xFC, 0xE0, 0x80, 0xE0, 0xFC, 0xFF, 0xFF, 0x3F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x1F, 0x7F, 0xFF, 0x7F, 0x1F, 0x03, 
	0x00, 0x00, 0x00, 0x00,   // 86 V
    0xFE, 0xFE, 0x00, 0xC0, 0xC0, 0x00, 0xFE, 0xFE, 0x0F, 0x1F, 0x18, 0x1F, 0x1F, 0x18, 0x1F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // 87 W
    0x3F, 0xFF, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x00, 0x00, 0x0F, 0x3F, 0xFF, 0xFC, 0xF0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 
	0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x3F, 0xFF, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F, 
	0x3F, 0xFF, 0xFC, 0xFC,   // 88 X
    0x07, 0x1F, 0x7E, 0xF8, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xF8, 0x7E, 0x1F, 0x07, 0x00, 0x00, 0x00, 0x03, 0x0F, 0x3E, 0xFC, 0xF0, 0xFC, 0x3F, 0x0F, 0x03, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00,   // 89 Y
    0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0xCF, 0xFF, 0xFF, 0xFF, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 
	0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xFF, 0x3F, 0x0F, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0xFF, 0xFF, 0xFF, 0xF3, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 
	0xF0, 0xF0, 0xF0, 0xF0,   // 90 Z
    0xFF, 0xFF, 0xFF, 0xFF, 0x1F, 0x0F, 0x0F, 0x0F, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xF8, 0xF0, 0xF0, 0xF0, 0xF0,   // 91 [
    0x07, 0x3F, 0xFF, 0xFC, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x0F, 0x7F, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F, 0x3F, 0xFF, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x0F, 
	0x3F, 0xFF, 0xFC, 0xE0,   // 92 "\"
    0x0F, 0x0F, 0x0F, 0x0F, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 
	0xF0, 0xF0, 0xF0, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF,   // 93 ]
    0xC0, 0xF0, 0xFC, 0x1F, 0xFC, 0xF0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,   // 94 ^
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0  // 95 _
// characters greater than 95 (0x5F) are not defined in this limited character set.
};

#endif
/*
There is some debate on the use of the font size in the data structure.
 In some cases it is used as an offset to the next font structure (which means it does have the size).
 In other cases it holds the actual address of the next font structure.
 Its only use within the Arduino DMD library is to determine whether or not the font is fixed or variable width:
  - zero indicates a variable width font and each character width is defined in the 'width' array
  - non zero indicates a fixed width font and its value is the character width. No 'width' array is then present.

Each character is made up of a fixed 16 pixels height by a variable number of pixels in width.
Each vertical column is represented by 2 bytes - the top 8 rows of pixels and the bottom 8 rows.
The top 8 rows are specified first, from left to right, followed by the lower 8 rows from left to right.
A '1' indicates the pixel is ON, and a '0' represents OFF.
The character width determines the number of bytes required for a character.
2 bytes are required per pixel in width in this font.
Fonts of 8 or fewer pixels in height require only 1 byte per pixel in width.
If the font 'size' is specified as "0x00, 0x00," then the font is fixed width and all character definitions
 must have the same number of bytes in their definition, and the width is specified in the font_Width.
 No font character width table is required or allowed in this case. 
The character definition consists of the list of data bytes starting from the top left byte through to
 the top right byte, followed by the lower left byte through to the lower right byte.

The example below shows how the character '1' is represented in this font.
Bytes and bits are numbered from 0 (not 1)'
 
   ______________ byte 0 = 0C  this byte is specified first
  |  ____________ byte 1 = 0E  followed by this one
  | |  __________ byte 2 = FF
  | | |  ________ byte 3 = FF
  | | | |  ______ byte 4 = FF
  | | | | |  ____ byte 5 = 00
  | | | | | |  __ byte 6 = 00
  | | | | | | |
      * * *     - bit 0  a '0' in bytes 0, 1, a '1' in bytes 3, 4, 5, and a '0' in bytes 5 & 6.
    * * * *     - bit 1
  * * * * *     - bit 2
  * * * * *     - bit 3
      * * *     - bit 4
      * * *     - bit 5                 Character '1' has a width of 0x07
      * * *     - bit 6                 Character '1' is represented by "0x0C, 0x0E, 0xFF, 0xFF,
      * * *     - bit 7                   0xFF, 0x00, 0x00, 0xE0, 0xE0, 0xFF, 0xFF, 0xFF, 0xE0, 0xE0"
      * * *     - bit 0
      * * *     - bit 1
      * * *     - bit 2
      * * *     - bit 3
      * * *     - bit 4
  * * * * * * * - bit 5
  * * * * * * * - bit 6
  * * * * * * * - bit 7
  ^ ^ ^ ^ ^ ^ ^
  | | | | | | |__ byte n   = E0  this byte is specified last
  | | | | | |____ byte n-1 = E0  this byte is specified 2nd last
  | | | | |______ byte n-2 = FF
  | | | |________ byte n-3 = FF
  | | |__________ byte n-4 = FF
  | |____________ byte n-5 = E0
  |______________ byte n-6 = E0  this byte is specified after all the upper bytes are specified

*/
