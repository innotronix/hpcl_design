
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Set the Text via Web Server
/*
 * The original source code to "running Text" : https://github.com/busel7/DMDESP/blob/master/examples/TeksDiamdanJalan/TeksDiamdanJalan.ino by  busel7
 * Links to download the DMDESP library : https://github.com/busel7/DMDESP
*/

//----------------------------------------Include Library
//----------------------------------------see here: https://www.youtube.com/watch?v=8jMr94B8iN0 to add NodeMCU ESP8266 library and board

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <FS.h>
#include <EEPROM.h>
#include <DMDESP.h>
#include "My32x8Font.h"
#include "My32x15Font.h"
#include "Arial_Black_16.h"
//#include <SoftwareSerial.h>

//SoftwareSerial swSer(4, 5, false, 128); //Define hardware connections
//(rx,tx);
//----------------------------------------

#include "PageIndex.h" //--> Include the contents of the User Interface Web page, stored in the same folder as the .ino file
//----------------------------------------Make a wifi name and password as access point

#ifndef APSSID
#define APSSID "HPCL RPDU 1.1"
#define APPSK  "HPCLRPDU1.1"
#endif

const char *ssid = APSSID;
const char *password = APPSK;

#define OTAUSER         "admin"    // Set OTA user
#define OTAPASSWORD     "admin"    // Set OTA password
#define OTAPATH         "/firmware"// Set path for update

#define RS485 2//

ESP8266WebServer server ( 80 );
ESP8266HTTPUpdateServer httpUpdater;
//----------------------------------------DMD Configuration (P10 Panel)
#define DISPLAYS_WIDE 3 //--> Panel Columns
#define DISPLAYS_HIGH 2 //--> Panel Rows
DMDESP Disp(DISPLAYS_WIDE, DISPLAYS_HIGH);  //--> Number of Panels P10 used (Column, Row)
//----------------------------------------

char *Text5[] = {"AAAA111"}; //--> Variable for scrolling Text. Don't leave this variable blank
String Incoming_Text = ""; //--> Variable to hold the text that is sent from a web server (web page)
String Petrol="PETROL";
String Diesel="DIESEL";
String Tbjet="TBJET";
String Power="POWER";
String CNG="CNG";
String Text4="";
String Text1="";
String Text2="";
String Text3="";
String  recieved_data="";
int display_time_m=0,display_y=0;

unsigned int y=0;
int promo1x=0;
  static uint32_t x10;
int incomingByte = 0;
char *Text[] = {"NodeMCU ESP8266 P10 LED Panel with DMDESP"}; //--> Variable for scrolling Text. Don't leave this variable blank
//========================================================================

//========================================================================VOID SETUP()
void setup(){
  Serial.begin(9600);
//  swSer.begin(9600);    //Initialize software serial with baudrate of 115200
  delay(500);
recieved_data="123156";
Serial.println(recieved_data);
  pinMode(RS485, OUTPUT);
  //----------------------------------------DMDESP Setup
  Disp.start(); //--> Run the DMDESP library
  Disp.setBrightness(200); //--> Brightness level
  //----------------------------------------
  
   EEPROM.begin(512);  //Initialize EEPROM
  httpUpdater.setup(&server, OTAPATH, OTAUSER, OTAPASSWORD);
  server.onNotFound(handleNotFound);
  server.begin();
  wifi_init();
Text2="133";
Text4="                                                                                                                                                              ";
     Text3=".88";
     incomingByte = 0;
  digitalWrite(RS485,LOW);
 
}
//========================================================================

//========================================================================VOID LOOP()
void loop(){
 
//  if(Serial.available() > 0) {  //wait for data at software serial
//    save_serial_data();
//    }
   server.handleClient();  //--> Handle client requests
   
   Disp.loop(); //--> Run "Disp.loop" to refresh the LED

  display_time_m++;
 main_display();

 //  Disp.loop(); //--> Run "Disp.loop" to refresh the LED
}


void main_display()
{
  //Serial.println('1');
  if(Serial.available() > 0) {  //wait for data at software serial
    
 recieved_data=Serial.readString();
 recieved_data.trim();
  //digitalWrite(RS485,HIGH);
  //Serial.println(recieved_data);
  digitalWrite(RS485,LOW);
    save_serial_data();
    }
  if((display_time_m>=0)&&(display_time_m<(2000+(2200*25))))
  {
    if(EEPROM.read(200)=='1')
    {
    Text1=Petrol;
    for(int xm=0;xm<3;xm++)
    {
      Text2[xm]=EEPROM.read(202+xm);
      Text3[xm]=EEPROM.read(205+xm);
    }
    if(Text2[0]=='0')
    {
      Text2[0]=' ';
    }
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Display_Text(0,40,55,69,78, 200,Text1,Text2,Text3); 
      else
      Display_Text(0,40,53,63,78, 200,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Display_Text(0,40,50,69,78, 200,Text1,Text2,Text3);
    else
      Display_Text(0,40,47,63,78, 200,Text1,Text2,Text3);
    }
    else if(EEPROM.read(200)=='0')
    {
      display_time_m=(17000+(2200*25));
    }
  }
  else if((display_time_m>=(2000+(2200*25)))&&(display_time_m<(2500+(2200*25))))
  {
   Disp.clear();
  }
  else if((display_time_m>=(2500+(2200*25)))&&(display_time_m<(17000+(2200*25))))
  {
     if(y>=32)
    {
     y=0;
      display_time_m=(17000+(2200*25));
    }
    
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Scrolling_Text(0,40,55,69,78, 60,Text1,Text2,Text3); 
      else
      Scrolling_Text(0,40,53,63,78, 60,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Scrolling_Text(0,40,50,69,78, 60,Text1,Text2,Text3);
    else
      Scrolling_Text(0,40,47,63,78, 60,Text1,Text2,Text3);
  
  }
   else if((display_time_m>=(17000+(2200*25)))&&(display_time_m<(17500+(2200*25))))
  {
   Disp.clear();
   y=0; 
  }
  else if((display_time_m>=(17500+(2200*25)))&&(display_time_m<(35000+(2200*25))))
  {
    if(EEPROM.read(210)=='1')
    {
    Text1=Diesel;
    for(int xm=0;xm<3;xm++)
    {
      Text2[xm]=EEPROM.read(212+xm);
      Text3[xm]=EEPROM.read(215+xm);
    }
    if(Text2[0]=='0')
    {
      Text2[0]=' ';
    }
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Display_Text(0,40,55,69,78, 200,Text1,Text2,Text3); 
      else
      Display_Text(0,40,53,63,78, 200,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Display_Text(0,40,50,69,78, 200,Text1,Text2,Text3);
    else
      Display_Text(0,40,47,63,78, 200,Text1,Text2,Text3);
    
    }
    else if(EEPROM.read(210)=='0')
    {
      display_time_m=(51500+(2200*25));
    }
  }
  else if((display_time_m>=(35000+(2200*25)))&&(display_time_m<(37000+(2200*25))))
  {
   Disp.clear();
  }
  else if((display_time_m>=(37000+(2200*25)))&&(display_time_m<(51500+(2200*25))))
  {
    if(y>=32)
    {
      y=0;
      display_time_m=(51500+(2200*25));
    }
    
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Scrolling_Text(0,40,55,69,78, 60,Text1,Text2,Text3); 
      else
      Scrolling_Text(0,40,53,63,78, 60,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Scrolling_Text(0,40,50,69,78, 60,Text1,Text2,Text3);
    else
      Scrolling_Text(0,40,47,63,78, 60,Text1,Text2,Text3);
  }
   else if((display_time_m>=(51500+(2200*25)))&&(display_time_m<(52000+(2200*25))))
  {
   Disp.clear();
   y=0; 
  }
  else if((display_time_m>=(52000+(2200*25)))&&(display_time_m<(69500+(2200*25))))
  {
    if(EEPROM.read(230)=='1')
    {
    Text1=Power;
    for(int xm=0;xm<3;xm++)
    {
      Text2[xm]=EEPROM.read(232+xm);
      Text3[xm]=EEPROM.read(235+xm);
    }
    if(Text2[0]=='0')
    {
      Text2[0]=' ';
    }
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Display_Text(0,40,55,69,78, 200,Text1,Text2,Text3); 
      else
      Display_Text(0,40,53,63,78, 200,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Display_Text(0,40,50,69,78, 200,Text1,Text2,Text3);
    else
      Display_Text(0,40,47,63,78, 200,Text1,Text2,Text3);
    
    }
    else if(EEPROM.read(230)=='0')
    {
      display_time_m=(84500+(2200*25));
    }
  }
  else if((display_time_m>=(69500+(2200*25)))&&(display_time_m<(70000+(2200*25))))
  {
   Disp.clear();
  }
  else if((display_time_m>=(70000+(2200*25)))&&(display_time_m<(84500+(2200*25))))
  {
    
    if(y>=32)
    {
      y=0;
      display_time_m=(84500+(2200*25));
    }
    
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Scrolling_Text(0,40,55,69,78, 60,Text1,Text2,Text3); 
      else
      Scrolling_Text(0,40,53,63,78, 60,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Scrolling_Text(0,40,50,69,78, 60,Text1,Text2,Text3);
    else
      Scrolling_Text(0,40,47,63,78, 60,Text1,Text2,Text3);
  }

  /* TurboJet*/
   else if((display_time_m>=(84500+(2200*25)))&&(display_time_m<(85000+(2200*25))))
  {
   Disp.clear();
   y=0; 
  }
  else if((display_time_m>=(85000+(2200*25)))&&(display_time_m<(102500+(2200*25))))
  {
    if(EEPROM.read(220)=='1')
    {
    Text1=Tbjet;
    for(int xm=0;xm<3;xm++)
    {
      Text2[xm]=EEPROM.read(222+xm);
      Text3[xm]=EEPROM.read(225+xm);
    }
    if(Text2[0]=='0')
    {
      Text2[0]=' ';
    }
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Display_Text(0,40,55,69,78, 200,Text1,Text2,Text3); 
      else
      Display_Text(0,40,53,63,78, 200,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Display_Text(0,40,50,69,78, 200,Text1,Text2,Text3);
    else
      Display_Text(0,40,47,63,78, 200,Text1,Text2,Text3);
    
    }
    else if(EEPROM.read(220)=='0')
    {
      display_time_m=(117500+(2200*25));
    }
  }
  else if((display_time_m>=(102500+(2200*25)))&&(display_time_m<(103000+(2200*25))))
  {
   Disp.clear();
  }
  else if((display_time_m>=(103000+(2200*25)))&&(display_time_m<(117500+(2200*25))))
  {
    if(y>=32)
    {
      y=0;
      display_time_m=(117500+(2200*25));
    }
    
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Scrolling_Text(0,40,55,69,78, 60,Text1,Text2,Text3); 
      else
      Scrolling_Text(0,40,53,63,78, 60,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Scrolling_Text(0,40,50,69,78, 60,Text1,Text2,Text3);
    else
      Scrolling_Text(0,40,47,63,78, 60,Text1,Text2,Text3);
  }
   /* CNG*/
   else if((display_time_m>=(117500+(2200*25)))&&(display_time_m<(118000+(2200*25))))
  {
   Disp.clear();
   y=0; 
  }
  else if((display_time_m>=(118000+(2200*25)))&&(display_time_m<(135500+(2200*25))))
  {
    if(EEPROM.read(240)=='1')
    {
    Text1=CNG;
    for(int xm=0;xm<3;xm++)
    {
      Text2[xm]=EEPROM.read(242+xm);
      Text3[xm]=EEPROM.read(245+xm);
    }
    if(Text2[0]=='0')
    {
      Text2[0]=' ';
    }
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Display_Text(0,40,55,69,78, 200,Text1,Text2,Text3); 
      else
      Display_Text(0,40,53,63,78, 200,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Display_Text(0,40,50,69,78, 200,Text1,Text2,Text3);
    else
      Display_Text(0,40,47,63,78, 200,Text1,Text2,Text3);
    
    }
    else if(EEPROM.read(240)=='0')
    {
      display_time_m=(150500+(2200*25));
    }
  }
  else if((display_time_m>=(135500+(2200*25)))&&(display_time_m<(136000+(2200*25))))
  {
   Disp.clear();
  }
  else if((display_time_m>=(136000+(2200*25)))&&(display_time_m<(150500+(2200*25))))
  {
    if(y>=32)
    {
      y=0;
      display_time_m=(150500+(2200*25));
    }
    
    if(Text2[1]=='1')
    {
      if(Text2[2]=='1')
      Scrolling_Text(0,40,55,69,78, 60,Text1,Text2,Text3); 
      else
      Scrolling_Text(0,40,53,63,78, 60,Text1,Text2,Text3); 
    }
    else if(Text2[2]=='1')
      Scrolling_Text(0,40,50,69,78, 60,Text1,Text2,Text3);
    else
      Scrolling_Text(0,40,47,63,78, 60,Text1,Text2,Text3);
  }
  
  else if((display_time_m>=(150500+(2200*25)))&&(display_time_m<(151000+(2200*25))))
  {
   Disp.clear();
   promo1x=EEPROM.read(199)+96;
  }
 else if((display_time_m>=(151000+(2200*25)))&&(display_time_m<(215000+((2200*promo1x)*25))))
  {
    if(EEPROM.read(198)==1)
    {
      int promo_len=EEPROM.read(199);
      //Serial.println(promo_len);
      for(int pr=0;pr<promo_len+1;pr++)
      {Text4[pr]=EEPROM.read(pr);}
     // Serial.println(Text4);
     Process_Incoming_Text();
    // if(x10<(promo_len+96))
    int fullScroll = Disp.textWidth(Text[0]) + 97;
     if(x10<fullScroll)
      horizontal_scroll(16, 50);
      else if(x10>fullScroll)
      display_time_m=(215000+((2200*promo1x)*25));
    }
    else if(EEPROM.read(198)==0)
    {
      display_time_m=(215000+((2200*promo1x)*25));
    }
  }
  else if((display_time_m>=(215000+((2200*promo1x)*25))))
  {
    y=0;
    x10=0;
    promo1x=0;
    display_time_m=0;  
    Disp.clear();
  } 
}


//========================================================================

void Display_Text(int x,int x1, int x2, int x4, int x5 ,uint8_t scrolling_speed,String Disp1,String Disp2,String Disp3)
{
 
  static uint32_t pM;
  if((millis() - pM) > scrolling_speed) { 
    pM = millis();
    Disp.setFont(My32x15Font);
    Disp.drawText(x, 9, Disp1);
    Disp.setFont(My32x15Font);
    Disp.drawChar(x1, 0, Disp2[0]);
    Disp.drawChar(x2, 0, Disp2[1]);
    Disp.drawChar(x4, 0, Disp2[2]);
    Disp.setFont(My32x8Font);
    Disp.drawText(x5, 9, Disp3);
  }  
}
//========================================================================Subroutines for scrolling Text
void Scrolling_Text(int x,int x1, int x2, int x4, int x5 ,uint8_t scrolling_speed,String Disp1,String Disp2,String Disp3)
{
  
  static uint32_t pM;
  int fullScroll = 32;
  if((millis() - pM) > scrolling_speed) { 
    pM = millis();
    if (y < fullScroll) {
      ++y;
    } else {
      y = 0;
      return;
    }
    Disp.setFont(My32x15Font);
    Disp.drawText(x, y+9, Disp1);
    Disp.setFont(My32x15Font);
    Disp.drawChar(x1, y, Disp2[0]);
    Disp.drawChar(x2, y, Disp2[1]);
    Disp.drawChar(x4, y, Disp2[2]);
    Disp.setFont(My32x8Font);
    Disp.drawText(x5, y+9, Disp3);
      
  }  
}

void Process_Incoming_Text() {
 // delay(500);
  //Serial.println("Incoming text : ");
  //Serial.println(Text4);
  //Serial.println();
  int str_len = Text4.length() + 1;
  char char_array[str_len];
  Text4.toCharArray(char_array, str_len);
  strcpy(Text[0], char_array);
 // Text4 = "";
}

void horizontal_scroll(int y, uint8_t scrolling_speed) {

   server.handleClient();  //--> Handle client requests
  static uint32_t pM;
  int width = Disp.width();
  Disp.setFont(Arial_Black_16);
  int fullScroll = Disp.textWidth(Text[0]) + width;
  if((millis() - pM) > scrolling_speed) { 
    pM = millis();
    if (x10 < fullScroll) {
      ++x10;
    } else {
      
      display_time_m=(215000+((2200*promo1x)*25));
     // x10 = 0;
      return;
    }
    Disp.drawText(width - x10, y-9, Text[0]);
  }  
}


void save_serial_data()
{
// recieved_data=Serial.readString();
// recieved_data.trim();
//  digitalWrite(RS485,HIGH);
//  Serial.println(recieved_data);
//  digitalWrite(RS485,LOW);
//*********************************************ID1(petrol)******************************************************************************  
 if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='1' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
      EEPROM.write(202,recieved_data[5]);
      EEPROM.commit();
      EEPROM.write(203,recieved_data[6]);
      EEPROM.commit();
      EEPROM.write(204,recieved_data[7]);
      EEPROM.commit();
      EEPROM.write(205,'.');
      EEPROM.commit();
      EEPROM.write(206,recieved_data[8]);
      EEPROM.commit();
      EEPROM.write(207,recieved_data[9]);
      EEPROM.commit(); 
 }
 else if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='2' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
      EEPROM.write(212,recieved_data[5]);
      EEPROM.commit();
      EEPROM.write(213,recieved_data[6]);
      EEPROM.commit();
      EEPROM.write(214,recieved_data[7]);
      EEPROM.commit();
      EEPROM.write(215,'.');
      EEPROM.commit();
      EEPROM.write(216,recieved_data[8]);
      EEPROM.commit();
      EEPROM.write(217,recieved_data[9]);
      EEPROM.commit(); 
 }
 else if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='3' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
      EEPROM.write(222,recieved_data[5]);
      EEPROM.commit();
      EEPROM.write(223,recieved_data[6]);
      EEPROM.commit();
      EEPROM.write(224,recieved_data[7]);
      EEPROM.commit();
      EEPROM.write(225,'.');
      EEPROM.commit();
      EEPROM.write(226,recieved_data[8]);
      EEPROM.commit();
      EEPROM.write(227,recieved_data[9]);
      EEPROM.commit(); 
 }
 else if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='4' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
      EEPROM.write(232,recieved_data[5]);
      EEPROM.commit();
      EEPROM.write(233,recieved_data[6]);
      EEPROM.commit();
      EEPROM.write(234,recieved_data[7]);
      EEPROM.commit();
      EEPROM.write(235,'.');
      EEPROM.commit();
      EEPROM.write(236,recieved_data[8]);
      EEPROM.commit();
      EEPROM.write(237,recieved_data[9]);
      EEPROM.commit(); 
 }
 else if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='5' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
      EEPROM.write(242,recieved_data[5]);
      EEPROM.commit();
      EEPROM.write(243,recieved_data[6]);
      EEPROM.commit();
      EEPROM.write(244,recieved_data[7]);
      EEPROM.commit();
      EEPROM.write(245,'.');
      EEPROM.commit();
      EEPROM.write(246,recieved_data[8]);
      EEPROM.commit();
      EEPROM.write(247,recieved_data[9]);
      EEPROM.commit(); 
 }
  else if(recieved_data[0]==':'&&recieved_data[1]=='A'&&recieved_data[2]=='P'&&recieved_data[3]=='M')
  {
    int str_len=recieved_data.length();
    digitalWrite(RS485,HIGH);
  Serial.println(str_len);
  digitalWrite(RS485,LOW);
  Serial.println(str_len);
  if(recieved_data[str_len-2]=='B'&&recieved_data[str_len-1]==';'){
    //:Serial.println("In promo_msg");
     for (int i=0; i<197;i++){
      EEPROM.write(i,0);
      EEPROM.commit();
     }
    for(int rec_im=0;rec_im<(str_len-6);rec_im++)
    {
    Serial.println("XXX");
   
     //message_store[rec_im]=promo_message[rec_im];
     EEPROM.write(rec_im,recieved_data[rec_im+4]);
      EEPROM.commit();
     Serial.println(recieved_data[rec_im+4]);
     //if(promo_message[rec_im]!=NULL)
     // message_store_count++;
    }
    EEPROM.write(199,(str_len-6));
    EEPROM.commit();
    
    int promo_flag=1;
    EEPROM.write(198,promo_flag);
    EEPROM.commit();
  }
  }
 else if(recieved_data[0]==':'&&recieved_data[1]=='A'&&recieved_data[2]=='R'&&recieved_data[3]=='M'&&recieved_data[4]=='P'&&recieved_data[5]=='M'&&recieved_data[6]=='B'&&recieved_data[7]==';'){
    //int str_len=recieved_data.length();
    for(int i=0;i<197;i++){
      EEPROM.write(i,0);
      EEPROM.commit();
    }
    int promo_flag=0;
    EEPROM.write(198,promo_flag);
    EEPROM.commit();
    //ESP.restart();
  }
 
}



void handleNotFound() {
  server.send(404, "text/plain", "404: Not found");
}

void handleRoot()
{

    
  String s = MAIN_page; //Read HTML contents
  server.send(200, "text/html", s); //Send web page
 
 
}
void handleUpdate()
{
 //---------------------------------------------Date& Time----------------------------------------------
  String timezone =server.arg("timezone");
  String date =server.arg("date");

 //---------------------------------------------Petrol----------------------------------------------
  String window1 =server.arg("window1");  
  if(window1!=NULL)
  {
    String Window_pet="";
    int pet_len=  window1.length();
    //Serial.print("String Length:");
    //Serial.println(pet_len);
    int index=window1.indexOf('.');
    //Serial.println(index);
    EEPROM.write(208,index);
    EEPROM.write(201,pet_len);
    EEPROM.commit();
    if(pet_len==5)
    {
      Window_pet='0'+window1;
    }
    else if(pet_len==6)
    {
      Window_pet=window1;
    }
    for(int pm=0;pm<6;pm++)
    {
      EEPROM.write(202+pm,Window_pet[pm]);
      EEPROM.commit();
    }
    //Serial.println(Window_pet);
  }
//----------------------------------------------Diesel------------------------------------------------
  String window2 =server.arg("window2");                  
  
  if(window2!=NULL)
  {
    String Window_pet="";
    int pet_len=  window2.length();
    //Serial.print("String Length:");
    //Serial.println(pet_len);
    int index=window2.indexOf('.');
    //Serial.println(index);
    EEPROM.write(218,index);
    EEPROM.write(211,pet_len);
    EEPROM.commit();
    if(pet_len==5)
    {
      Window_pet='0'+window2;
    }
    else if(pet_len==6)
    {
      Window_pet=window2;
    }
    for(int pm=0;pm<6;pm++)
    {
      EEPROM.write(212+pm,Window_pet[pm]);
      EEPROM.commit();
    }
    //Serial.println(Window_pet);
  }
  //---------------------------------------------X-Mile-------------------------------------------------
  String window3 =server.arg("window3");

 if(window3!=NULL)
  {
    String Window_pet="";
    int pet_len=  window3.length();
    //Serial.print("String Length:");
    //Serial.println(pet_len);
    int index=window3.indexOf('.');
    //Serial.println(index);
    EEPROM.write(238,index);
    EEPROM.write(231,pet_len);
    EEPROM.commit();
    if(pet_len==5)
    {
      Window_pet='0'+window3;
    }
    else if(pet_len==6)
    {
      Window_pet=window3;
    }
    for(int pm=0;pm<6;pm++)
    {
      EEPROM.write(232+pm,Window_pet[pm]);
      EEPROM.commit();
    }
    //Serial.println(Window_pet);
  }
  //------------------------------------------X-Premium----------------------------------------------------
  String window4 =server.arg("window4");                  

 
 if(window4!=NULL)
  {
    String Window_pet="";
    int pet_len=  window4.length();
    //Serial.print("String Length:");
    //Serial.println(pet_len);
    int index=window4.indexOf('.');
    //Serial.println(index);
    EEPROM.write(228,index);
    EEPROM.write(221,pet_len);
    EEPROM.commit();
    if(pet_len==5)
    {
      Window_pet='0'+window4;
    }
    else if(pet_len==6)
    {
      Window_pet=window4;
    }
    for(int pm=0;pm<6;pm++)
    {
      EEPROM.write(222+pm,Window_pet[pm]);
      EEPROM.commit();
    }
    //Serial.println(Window_pet);
  }
//------------------------------------------CNG----------------------------------------------------
  String window6 =server.arg("window6");
  
 if(window6!=NULL)
  {
    String Window_pet="";
    int pet_len=  window6.length();
    //Serial.print("String Length:");
    //Serial.println(pet_len);
    int index=window6.indexOf('.');
    //Serial.println(index);
    EEPROM.write(248,index);
    EEPROM.write(241,pet_len);
    EEPROM.commit();
    if(pet_len==5)
    {
      Window_pet='0'+window6;
    }
    else if(pet_len==6)
    {
      Window_pet=window6;
    }
    for(int pm=0;pm<6;pm++)
    {
      EEPROM.write(242+pm,Window_pet[pm]);
      EEPROM.commit();
    }
    //Serial.println(Window_pet);
  }                  
   //----------------------------------------http------------------------------------------------------
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?timezone=" + timezone  + "&date=" + date + "&window1=" + window1 + "&window2=" + window2 + "&window3=" + window3 + "&window4=" + window4 +"&window6=" + window6 + "&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(400,"text/html",response); 
}

//-----------------------------------Start of Function handleUpdate1--------------------------------- 
void handleUpdate1()
{
 //*********************************************line 1 add/remove product****************************************************************************** 
 String window7 =server.arg("window7"); 
 if(window7[0]!=NULL && window7[1]!=NULL )
  {
   if(window7[0]=='P'&& window7[1]=='+')
   {
     EEPROM.write(200,'1');
     EEPROM.commit();
   }
   else if(window7[0]=='P'&& window7[1]=='-')
   {
     EEPROM.write(200,'0');
     EEPROM.commit();
   }
   else if(window7[0]=='D'&& window7[1]=='+')
   {
     EEPROM.write(210,'1');
     EEPROM.commit();
   }
   else if(window7[0]=='D'&& window7[1]=='-')
   {
     EEPROM.write(210,'0');
     EEPROM.commit();
   }
   else if(window7[0]=='O'&& window7[1]=='+')
   {
     EEPROM.write(230,'1');
     EEPROM.commit();
   }
   else if(window7[0]=='O'&& window7[1]=='-')
   {
     EEPROM.write(230,'0');
     EEPROM.commit();
   }
   else if(window7[0]=='T'&& window7[1]=='+')
   {
     EEPROM.write(220,'1');
     EEPROM.commit();
   }
   else if(window7[0]=='T'&& window7[1]=='-')
   {
     EEPROM.write(220,'0');
     EEPROM.commit();
   }
   else if(window7[0]=='C'&& window7[1]=='+')
   {
     EEPROM.write(240,'1');
     EEPROM.commit();
   }
   else if(window7[0]=='C'&& window7[1]=='-')
   {
     EEPROM.write(240,'0');
     EEPROM.commit();
   }
  }
 String window8 =server.arg("window8"); 
 String window9 =server.arg("window9"); 
 //String product_set =server.arg("product_set");  
  String product_set =server.arg("product_set");  
 unsigned int num = product_set.length();
   Serial.print("String length is: ");
   Serial.println(num);
   EEPROM.write(238,num);
      EEPROM.commit();
 
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window7=" + window7 + "&window8=" + window8+ "&product_set=" + product_set +"&window9=" + window9 +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(200,"text/html",response);
 //----------------------------------------------------------------------------------------------

}
void handleUpdate2()
{
 
 //*********************************************Promotional Message****************************************************************************** 
 String promo_message =server.arg("promo_message");
 int promo_html_len=promo_message.length();
 Serial.println(promo_html_len);
 if(promo_message[0]!=NULL)
  {
     for (int i=0; i<197;i++){
      EEPROM.write(i,0);
      EEPROM.commit();
     }
    for(int rec_im=0;rec_im<promo_html_len;rec_im++)
    {
    
     //message_store[rec_im]=promo_message[rec_im];
     EEPROM.write(rec_im,promo_message[rec_im]);
      EEPROM.commit();
     Serial.println(promo_message[rec_im]);
     //if(promo_message[rec_im]!=NULL)
     // message_store_count++;
    }
    EEPROM.write(199,promo_html_len);
    EEPROM.commit();
  }

  //----------------------------------------------------------------------------------------------   
  String message_f=server.arg("message_f");
  int message_flag=0;
  if(message_f[0]!=NULL && message_f[1]!= NULL)
  {
    message_flag=1;
    EEPROM.write(198,message_flag);
     EEPROM.commit();
    
  }  
  else
  {
    message_flag=0;
    EEPROM.write(198,message_flag); 
     EEPROM.commit();
  }

  
  //----------------------------------------http------------------------------------------------------
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?promo_message=" + promo_message + "&message_f=" + message_f  +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(200,"text/html",response);
}


void handleInit() 
{
  unsigned char y=0;
  String window1= "      ";     
  String window2 = "      ";
  String window3 = "      ";
  String window4 = "      ";
  String response;
  //conn_f=1;
  //Serial.println("In handleInit bufferOut");
  Serial.println(window1);
  Serial.println(window2);
  Serial.println(window3);
  Serial.println(window4);

  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window1=" + window1 + "&window2=" + window2 + "&window3=" + window3 + "&window4=" + window4 +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  response = start + mid1 + end1 ;
  server.send(200,"text/html",response);
  
}



void wifi_init()
{
 //***********************ssid and password********************************************************
 
      char ssid1[30];
      char id1[30];
      char NRValue[8];
      char product_name2[4]={0};
      char id_health[3]={0};
      unsigned int id_value;
      String(ESP.getChipId()).toCharArray(NRValue,8);
      strcpy(ssid1,ssid);
     /* if(EEPROM.read(238)!=255){id_value=EEPROM.read(238);}
      else{id_health[0]='0';}
      strcpy(id1,"-ILT-");
      if(id_value>0)
      {
        for(int i=0;i<id_value;i++)
        {
          if(EEPROM.read(235+i)!=255){product_name2[i]=EEPROM.read(235+i);}
          else{product_name2[i]='0';}
        }
      }
      else if(id_value<=0)
      {  
        product_name2[0]='0';
      }
      strcat(ssid1,product_name2);
      strcat(ssid1,id1);
      strcat(ssid1,NRValue);
      Serial.println(product_name2);*/
      //strcat(ssid1,product_name2);
      //strcat(ssid1," ID");
      Serial.print("SSID:");
      Serial.println(ssid1);
      WiFi.mode(WIFI_AP);           //Only Access point
      WiFi.softAP(ssid1, password); //Start HOTspot removing password will disable security
      IPAddress myIP = WiFi.softAPIP(); //Get IP address
      Serial.print("HotSpt IP:");
      Serial.println(myIP);
      
//-------------------------------------------------------------------------------------------------------------
 /* if (!SPIFFS.begin())
  {
    // Serious problem
    Serial.println("SPIFFS Mount failed");
  } else {
    Serial.println("SPIFFS Mount succesfull");
  }
  server.serveStatic("/img", SPIFFS, "/img");
  server.serveStatic("/", SPIFFS, "/index1.html");*/
//--------------------------------------------------------------------------------------------------------------
  server.begin();
  server.on("/", handleRoot);
  server.on("/update", handleUpdate);
  server.on("/init", handleInit);
  //server.on("/index.html",handleInit);
  server.on("/update1", handleUpdate1);
  server.on("/update2", handleUpdate2);
  //server.on("/setPWM", handle_pwm);
    
}
